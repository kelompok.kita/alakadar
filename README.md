 
 
Raden Salsabilla Hisyana Z-1806185411
Aisyah Amyra Izra - 1806235706
Fadhil Ilham Rahmadi- 1806191194
Adriel Gian Ananta-1806186811

•	link herokuapp : alakadar.herokuapp.com


•	Cerita aplikasi yang diajukan serta kebermanfaatannya

Kami, mengajukan website ‘Alakadar’ alias ala kantin bundar yang berguna untuk memesan makanan di kantin fasilkom. Karena kantin fasilkom sangat ramai di jam jam tertentu, terkadang pembeli memiliki keluhan untuk memesan makanan langsung ke kantinnya seperti ‘sumpek’, ‘panas’, ‘lama’ atau ada deadline yang harus dikejar sehingga waktu sangatlah penting. Kami membuat website ini sehingga pembeli tidak perlu datang mengantri ke kantin untuk memesan makanan dan hanya cukup  mengambilnya jika makanan sudah jadi. Dengan begitu, keluhan2 seperti yang disebutkan sebelumnya diharapkan berkurang. Selain itu kami juga membantu para penjual agar tidak bingung pesanan dari pembelinya, karena terkadang saking banyak pembelinya, penjual seringkali lupa apa saja pesanan yang harus dibuatnya.

•	Daftar fitur yang akan diimplementasikan
-Daftar Penjual di Kantin
-Daftar Makanan di setiap toko
-Pembeli pilih makanan  : pesan, catatan
(akan ada list makanan yg dipesan beserta statusnya)
-Penjual melihat pesanan yang masuk : proses, tolak
 (akan ada list pesanan untuk toko tesb)



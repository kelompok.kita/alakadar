# Generated by Django 2.2.6 on 2019-10-19 13:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('EditMenu', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='menu',
            name='harga',
            field=models.PositiveIntegerField(default=0),
        ),
    ]

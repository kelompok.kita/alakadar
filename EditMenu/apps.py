from django.apps import AppConfig


class EditmenuConfig(AppConfig):
    name = 'EditMenu'

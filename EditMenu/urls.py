from django.urls import include, path
from . import views

app_name = 'edit'

urlpatterns = [
    path('', views.edit, name='edit'),
    path('edit_post', views.edit_post, name='edit_post'),
    path('delete/<int:delete_id>', views.delete, name='delete'),
]
from django.shortcuts import render, redirect
from .models import Menu
from .forms import edit_menu

# Create your views here.
def edit(request):
    edit = Menu.objects.all().values()

    response = {
        'form' : edit_menu,
        'edit' : edit
    }

    return render(request, 'EditMenu.html', response)


def edit_post(request):
    form = edit_menu(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        form.save()
        return redirect('edit:edit')
    
    else:
        return redirect('edit:edit')

def delete(request, delete_id):
    Menu.objects.filter(id=delete_id).delete()
    return redirect('edit:edit')
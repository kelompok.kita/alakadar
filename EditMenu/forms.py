from django import forms
from .models import Menu

class edit_menu(forms.ModelForm):
    class Meta:
        model = Menu
        fields = '__all__'



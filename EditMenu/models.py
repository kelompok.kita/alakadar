from django.db import models

# Create your models here.

class Menu(models.Model):
    nama_toko = models.ForeignKey('LoginPenjual.Toko', on_delete=models.SET_NULL, null=True)
    nama_menu = models.CharField(max_length=100)
    harga = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.nama_menu
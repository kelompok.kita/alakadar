from django.db import models

# Create your models here.
class Toko(models.Model):
    nama_penjual = models.CharField(max_length=100)
    nama_toko = models.CharField(max_length=100)

    def __str__(self):
        return self.nama_toko

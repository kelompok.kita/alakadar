from django.urls import path
from . import views

app_name = 'login'

urlpatterns = [
    path('', views.login_penjual, name='login'),
    path('login_post', views.login_post, name='login_post'),
]
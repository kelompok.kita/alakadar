from django.shortcuts import render, redirect
from .models import Toko
from .forms import login_form

# Create your views here.
def login_penjual(request):
    login = Toko.objects.all().values()

    response = {
        'form' : login_form,
        'login' : login
    }

    return render(request, 'LOGIN.html', response)

def login_post(request):
    form = login_form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        form.save()
        return redirect('login:login')
    
    else:
        return redirect('login:login')
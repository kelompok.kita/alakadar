from django import forms
from .models import Toko

class login_form(forms.ModelForm):
    class Meta:
        model = Toko
        fields = '__all__'
from django.shortcuts import render, redirect
from .models import Pesan
from .forms import pesan_form

# Create your views here.
def pesan(request):
    pesan = Pesan.objects.all().values()

    response = {
        'form' : pesan_form,
        'pesan' : pesan
    }

    return render(request, 'canteen.html', response)

def pesan_post(request):
    form = pesan_form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        form.save()
        return redirect('pesan:pesan')
    
    else:
        return redirect('pesan:pesan')

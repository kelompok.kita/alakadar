from django.db import models
from django.utils import timezone
from datetime import datetime, date
from LoginPenjual.models import Toko
from EditMenu.models import Menu

class Pesan(models.Model):
    toko = models.ForeignKey('LoginPenjual.Toko', on_delete=models.SET_NULL, null=True)
    menu = models.ForeignKey('EditMenu.Menu', on_delete=models.SET_NULL, null=True)
    kuantitas = models.PositiveIntegerField(default=0)

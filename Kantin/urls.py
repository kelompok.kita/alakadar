from django.urls import path
from . import views

app_name = 'pesan'

urlpatterns = [
    path('', views.pesan, name='pesan'),
    path('pesan_post', views.pesan_post, name='pesan_post')
]
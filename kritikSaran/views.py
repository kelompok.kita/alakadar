from django.shortcuts import render,redirect
from .models import Schedule
from . import forms

# Create your views here.
def schedule(request):
    schedules = Schedule.objects.all()
    return render(request, 'schedule.html', {'schedules': schedules})

def createsched(request):
    if request.method == 'POST':
        form = forms.ScheduleForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('kritikSaran:schedule')

    else:
        form = forms.ScheduleForm()
    return render(request, 'createsched.html', {'form': form})

def schedule_delete(request):
	Schedule.objects.all().delete()
	return render(request, "schedule.html")
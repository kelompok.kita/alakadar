from django.urls import path
from . import views
from . import models

app_name = 'kritikSaran'

urlpatterns = [
    path('', views.schedule, name='schedule'),
    path('createsched/', views.createsched, name='createsched'),
    path('schedule/delete', views.schedule_delete, name='schedule_delete'),
]


